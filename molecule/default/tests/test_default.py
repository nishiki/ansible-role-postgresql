import testinfra.utils.ansible_runner

def test_packages(host):
  for package_name in ['postgresql-15', 'python3-psycopg2']:
    package = host.package(package_name)
    assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/postgresql/15/main/postgresql.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains("listen_addresses = '*'")

def test_hba_file(host):
  path = host.file('/etc/postgresql/15/main/pg_hba.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('local "myappli","myappli2" all md5')

def test_service(host):
  service = host.service('postgresql@15-main')
  assert service.is_running
  assert service.is_enabled

def test_socket(host):
  socket = host.socket('tcp://127.0.0.1:5432')
  assert socket.is_listening

def test_database(host):
  cmd = host.run('su - postgres -c "psql -c \'\\l\'"')
  assert cmd.succeeded
  assert 'myappli' in cmd.stdout

def test_user(host):
  cmd = host.run('su - postgres -c "psql -c \'\\du\'"')
  assert cmd.succeeded
  assert 'myuser' in cmd.stdout

def test_grants(host):
  cmd = host.run('PGPASSWORD=secret psql -U myuser -d myappli -c \'create table test ( col text);\'')
  assert cmd.succeeded
  assert 'CREATE TABLE' in cmd.stdout
