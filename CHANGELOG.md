# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Changed

- break: new system for user and privileges
- break: default postgresql version is 15
- feat: remove apt_key use
- test: use personal docker registry

### Added

- add database owner
- add variable postrgresql_primary
- add user attributes
- test: add support debian 12

### Fixed

- fix replication database in pg_hba
- fix pg_hba when multiple users or multiple databases

### Removed

- test: remove support debian 10
- test: remove support debian 11

## v1.0.0 - 2021-09-10

- first version
